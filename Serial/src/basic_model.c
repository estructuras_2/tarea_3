#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include <basic_model.h>

#define immunity_duration 4     //number of ticks for immunity duration
#define initial_infected 10     //Amount of sick agents from the beginning
#define duration 5              //Virus duration
#define recover_chances 0.5     //Minimum chances needed for an agent to recover
#define grid 50                 //Grid size
#define reproduce_chance 0.5    //Mimimum chances
#define infectiousness 0.75     //Chances of the agents of getting sick.
#define initial_sick_time 10    //Initial sick time for an agent who starts sick at the beggining

/*Global variables*/
int lifespan;                                      //the lifespan of an agent
float infected;                                    //what % of the population is infectious
float chance_reproduce;                            //the probability of an agent generating an offspring each tick
float immune;                                      //what % of the population is immune
int deaths;                                        //Number of agents that have died
const char *output_file = "simulation_report.txt"; //Nambe for output file

struct agent_vars setup_agents(int lifespan, int carrying_capacity)
{
    /*
    Creates a new agent and sets all its values for the simulation.
    */

    struct agent_vars new_agent;
    new_agent.lifespan = rand() % lifespan;                //Random lifespan from 0 to 100 years
    new_agent.chance_reproduce = RandomFloat(0, 1);   //Probability of reproduction each tick
    new_agent.sick = rand() % 1;                      //Randomly sets if the agent is sick or not
    if(new_agent.sick == 1){
        new_agent.remaining_immunity = 0;             //Randomly sets an immunity time for an agent
        new_agent.sick_time = random() % initial_sick_time;          //Sets an random sick time
    }
    else{
        new_agent.remaining_immunity = rand() % 4;    //Randomly sets an immunity time for an agent
        new_agent.sick_time = 0;
    }
    new_agent.age = rand() % (lifespan/2);            //Random lifespan from 0 to lifespan
    new_agent.x_pos = random_pos();                   //Random x position  
    new_agent.y_pos = random_pos();                   //Random y position
    new_agent.alive = 1;                              //Sets the agant as alive
    new_agent.chance_recover = RandomFloat(0, 1);     //Probability to recover
    return new_agent;
}

float RandomFloat(int a, int b)
{
    /*
    Creates a random float value between two given integers
    */
    
    float random = ((float)rand()) / (float)RAND_MAX; 
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

int random_pos() 
{
    /*
    This sets a random number for a position
    */

    int pos = rand() % grid; //Choose random position inside the grid
    return pos;
}

struct agent_vars *create_population(int lifespan, int carrying_capacity)
{
    /*
    This creates a "dynamic array" fills it with the agents
    */

    struct agent_vars* population;                                      //agent_vars pointer type
    population = calloc(carrying_capacity, sizeof(struct agent_vars));  //Ask for memory to allocate the agents
    for(int i = 0; i < carrying_capacity; i++){
        *(population + i) = setup_agents(lifespan, carrying_capacity);  //Creates a new agents and stores it on memory
    }

    /*
    This chooses 10 random agents to infect them
    */

    int random;                                 //Declaration for a random variables
    for(int j = 0; j < initial_infected; j++){
        random = rand() % carrying_capacity;    //Choose a number randomly
        get_sick((population + random));        //Infect the agent on the random position
    }
    return population;
}

void get_sick(struct agent_vars *agent)
{
    /*
    It takes an agent and sets its parameters sick and immunity to 1 and 0 respectively
    */

    agent->sick = 1;
    agent->remaining_immunity = 0;
}

void get_healthy(struct agent_vars *agent)
{
    /*
    It takes an agent and sets its parameters sick, sicktime and remaining_immunity
    to 0,0 and 0 respectively
    */

    agent->sick = 0;
    agent->remaining_immunity = 0;
    agent->sick_time = 0;
}

void become_immune(struct agent_vars *agent)
{
    /*
    It takes an agent and sets its parameters sick, sicktime and remaining_immunity
    to 0,0 and immunity_duration respectively
    */

    agent->sick = 0;
    agent->sick_time = 0;
    agent->remaining_immunity = immunity_duration;
}        

void get_killed(int position,struct agent_vars* population)
{
    /*
    This changes the alive flag to 0 for a specific agent
    */

    (population + position)->alive = 0;
}

void get_older(struct agent_vars* population, int carrying_capacity, int lifespan)
{
    /*
    Checks if the agent is alive, then updates its parameters. This function is parallelized
    */

    for(int i = 0; i < carrying_capacity; i++){
        if((population + i)->alive == 1){                   //Checks if the agent is alive
            (population + i)->age += 1;                     //Increase the agent's age
            if((population + i)->sick == 1){                //Checks if the agent is sick
                (population + i)->sick_time += 1;           //Increases agent's sick time
            }
            if((population + i)->remaining_immunity > 0 ){  //Checks if the agent still being immune
                (population + i)->remaining_immunity -= 1;  //Decreases agant's sick time
            }
            if((population + i)->age > lifespan){           //Checks if the agent is on its maximum age
                get_killed(i,population);   //I it it, it gets killed
            }
        }
    }
}

void move(struct agent_vars* population, int carrying_capacity)
{
    /*
    Updates the position of the agents to a new random one
    */

    for(size_t i= 0; i < carrying_capacity; i++){
        (population + i)->x_pos = rand() % grid;    //Sets a random x position
        (population + i)->y_pos = rand() % grid;    //Sets a random y position
    }
}

void update_global_variables(struct agent_vars* population, int carrying_capacity, struct global_variables *vars)
{
    /*
    Counts the total immune and totla infected agents to update the simmulation.
    */

    int alive_counter = 0;      //Alive agents counter
    int sick_counter = 0;       //Sick agents counter
    int immune_counter = 0;     //Immune agents counter
    int healthy_counter = 0;    //Healthy agents counter
    for(int i = 0; i < carrying_capacity; i++){
        if((population + i)->alive == 1){                       //Checks if the agent is alive
            alive_counter += 1;                                 //Increases the alive counter
            if((population + i)->sick == 1){                    //checks if the agent is sick
                sick_counter += 1;                              //Increases the sick counter
            }
            else{
                healthy_counter += 1;                           //Increases the healthy counter
                if((population + i)->remaining_immunity > 0){   //Checks if the agent still being immune
                    immune_counter += 1;                        //Increase immune counter
                }
            }
        }
    }
    vars->total_sick = sick_counter;        //Assign sick counter to the structure
    vars->total_immune = immune_counter;    //Assign immune counter to the structure
    vars->total_healthy = healthy_counter;  //Assign healthy counter to the structure
    vars->alive_count = alive_counter;      //Assign alive counter to the structure
}

void reproduce(struct agent_vars* population, int lifespan, int carrying_capacity)
{
    //Create a new agent if there's less than the maximum agents number and if there's 2 with chances over 70%*/

    int dead_pos;               //Dead agent position
    int dead_count = 0;         //Dead counter
    int reproduce_counter = 0;  //Reproduce counter
    for(int i = 0; i < carrying_capacity; i++){
        if((population + i)->alive == 0){                               //Checks if the agent is dead
            dead_pos = i;                                               //Save the dead agent's position
            dead_count ++;                                              //Increases the dead counter
        }
        else{
            if((population + i)->chance_reproduce > reproduce_chance){  //Checks if the agent has enought reproduce chances
                reproduce_counter ++;                                   //Increase reproduce counter
            }
        }
    }
    if((dead_count > 0) && (reproduce_counter > 0)){                    //Checks if there are dead agents and agents that can reproduce
        struct agent_vars baby_agent = setup_agents(lifespan, carrying_capacity);   //Creates a new agent
        get_healthy(&baby_agent);               //Sets the new agent as healthy
        *(population + dead_pos) = baby_agent;  //Inserts the new agent on the dead agent's position
    }
}

void recover_or_die(struct agent_vars* population, int carrying_capacity)
{
    /*
    This searches for agents that have overpassed the virus duration and sees if they have chances to recover.
    In case they don't have enough chance, they'll get killed.
    */

    for(int i = 0; i < carrying_capacity; i++){
        if((population + i)->alive){                                    //Checks if the agent is alive
            if((population + i)->sick_time >= duration){                //Checks if the agent has overpassed the virus time
                if((population + i)->chance_recover > recover_chances){ //Checks if the agent has enough chance to recover
                    become_immune(population + i);  //The agent gets cured
                }
                else{
                    get_killed(i,population + i);   //The agent gets killed
                }
            }
        }
    }
}

void printer(int max_weeks, int carrying_capacity, int total_sick, int total_immune, int total_healthy, int total_agents, int week_num){
    /* This function handles the output file printing the variables in a txt file.
        Inputs: 
            max_weeks: maximum ammount of weeks (printed only once).
            carrying_capacity: maximum ammount of agents populating the grid.
            total_sick: total ammount of sick agents.
            total_immune: total ammount of immune agents.
            total_healthy: total ammount of sick agents.
            total_agent: total ammount of agents.
            week_num: current week (tick).
    */

    FILE *fp;
    fp = fopen("simulation_report.txt", "a");
    if (week_num <= max_weeks){
        if (week_num == 0){
            remove("simulation_report.txt");
            fp = fopen("simulation_report.txt", "a");
            fprintf(fp, "carrying capacity: %d\n", carrying_capacity);
            fprintf(fp, "max_weeks: %d\n", max_weeks);
        }
        fprintf(fp, "tick[%d] %d,%d,%d,%d\n", week_num, total_sick, total_immune, total_healthy, total_agents);
    }
    fclose(fp);
}

void infect(struct agent_vars* population, int total_agents)
{

    /*      
        This function checks the sick condition of an agent and infects the surrounding agents with no immunity as shown:
            O   O   O        
            O   X   O
            O   O   O
            Where X is the current agent (infected one) and O are the sorrounding agents (prompted to get sick if not sick already or not immune).
        
            Inputs:
                population: struct of the entire grid of agents.
                total_agents: ammount of agents in the current ticks.
            
    */

    //initial values for excluding the first iteration case in wich no one is needed to get sick. (negative value for unlogical case).
    int pos_x = -1;
    int pos_y = -1;
    int right_pos = -1;
    int left_pos = -1;
    int upper_pos = -1;
    int down_pos = -1;
    int infectious = 0;

    //Pointer for saving values in each iteration of the for loop.
    int *point_pos_x = NULL;
    int *point_pos_y = NULL;
    int *point_right_pos = NULL;
    int *point_left_pos = NULL;
    int *point_pos_upper = NULL;
    int *point_pos_down = NULL;
    int *point_infectious = NULL;
    point_pos_x = &pos_x;
    point_pos_y = &pos_y;
    point_right_pos = &right_pos;
    point_left_pos = &left_pos;
    point_pos_upper = &upper_pos;
    point_pos_down = &down_pos;
    point_infectious = &infectious;
    
    for (int i = 0; i < total_agents; i++)
    {
        if (((population + i)->sick) == 1)
        {
            *(point_pos_x) = (population + i)->x_pos;
            *(point_pos_y) = (population + i)->y_pos;
            *(point_right_pos) = *(point_pos_x) + 1;
            *(point_left_pos) = *(point_pos_x) - 1;
            *(point_pos_upper) = *(point_pos_y) + 1;
            *(point_pos_down) = *(point_pos_y) - 1;
            continue;
        }
        if (((population + i)->sick) == 0 && *point_pos_x != -1) {
            if ((population + i)->x_pos == *point_right_pos && (population + i)->y_pos == *point_pos_y && (population + i)->remaining_immunity == 0)
            {
                *point_infectious = RandomFloat(1,0);
                if (*point_infectious <= infectiousness){
                    get_sick((population + i));
                }
            }
            if ((population + i)->x_pos == *point_left_pos && (population + i)->y_pos == *point_pos_y && (population + i)->remaining_immunity == 0)
            {
                *point_infectious = RandomFloat(1,0);
                if (*point_infectious <= infectiousness){
                    get_sick((population + i));
                }
            }
            if ((population + i)->x_pos == *point_pos_x && (population + i)->y_pos == *point_pos_upper && (population + i)->remaining_immunity == 0)
            {
                *point_infectious = RandomFloat(1,0);
                if (*point_infectious <= infectiousness){
                    get_sick((population + i));
                }
            }
            if ((population + i)->x_pos == *point_pos_x && (population + i)->y_pos == *point_pos_down && (population + i)->remaining_immunity == 0)
            {
                *point_infectious = RandomFloat(1, 0);
                if (*point_infectious <= infectiousness)
                {
                    get_sick((population + i));
                }
            }
            if ((population + i)->x_pos == *point_right_pos && (population + i)->y_pos == *point_pos_upper && (population + i)->remaining_immunity == 0)
            {
                *point_infectious = RandomFloat(1, 0);
                if (*point_infectious <= infectiousness)
                {
                    get_sick((population + i));
                }
            }
            if ((population + i)->x_pos == *point_left_pos && (population + i)->y_pos == *point_pos_upper && (population + i)->remaining_immunity == 0)
            {
                *point_infectious = RandomFloat(1, 0);
                if (*point_infectious <= infectiousness)
                {
                    get_sick((population + i));
                }
            }
            if ((population + i)->x_pos == *point_right_pos && (population + i)->y_pos == *point_pos_down && (population + i)->remaining_immunity == 0)
            {
                *point_infectious = RandomFloat(1, 0);
                if (*point_infectious <= infectiousness)
                {
                    get_sick((population + i));
                }
            }
            if ((population + i)->x_pos == *point_left_pos && (population + i)->y_pos == *point_pos_down && (population + i)->remaining_immunity == 0)
            {
                *point_infectious = RandomFloat(1, 0);
                if (*point_infectious <= infectiousness)
                {
                    get_sick((population + i));
                }
            }
            if ((population + i)->x_pos == *point_pos_x && (population + i)->y_pos == *point_pos_y && (population + i)->remaining_immunity == 0)
            {
                *point_infectious = RandomFloat(1, 0);
                if (*point_infectious <= infectiousness)
                {
                    get_sick((population + i));
                }
            }
        }
    }
}