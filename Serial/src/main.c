#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include <basic_model.h>
#include <time.h>

int main(int argc, char * argv []){
    double timespent = 0.0;
    clock_t begin = clock();
    int lifespan = atoi(argv[1]);             //Lifespan of te agents as param
    int max_weeks = atoi(argv[2]);            //Ammount of ticks in the run as param
    int carrying_capacity = atoi(argv[3]);    //Carrying_capacity of ticks in the run as param.
    int week_num = 0;                         //Number of week (tick) of the iteration process.
    struct global_variables vars;             //Structure for the global variables
    struct agent_vars *population = create_population(lifespan, carrying_capacity); //Creates a population
  
    // MAIN LOOP!!!!
    for(int j = 0; j < max_weeks; j++ ){  
        week_num = j;
        infect(population, carrying_capacity);
        recover_or_die(population, carrying_capacity);
        reproduce(population, lifespan,carrying_capacity);
        get_older(population, carrying_capacity, lifespan);
        update_global_variables(population, carrying_capacity, &vars);
        printer(max_weeks, carrying_capacity, vars.total_sick, vars.total_immune, vars.total_healthy, vars.alive_count, week_num);
        move(population, carrying_capacity);
    }
    clock_t end = clock();
    timespent += (double)(end-begin)/CLOCKS_PER_SEC;
    printf("Execution time: %f s \n",timespent);
}