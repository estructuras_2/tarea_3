#ifndef PTHREADS
#define PTHREADS

struct agent_vars
{
    //Struct with the characteristics for every agente we generate.

    int lifespan;           //Agent's lifetime
    float chance_reproduce; //Agent's chances to reproduce
    int sick;               //1: sick, 0: healthy 
    int remaining_immunity; //weeks of immunity left for an agent
    int sick_time;          //weeks the agent has been sick
    int age;                //weeks old for every agent
    int x_pos;              //Agent's x position
    int y_pos;              //Agent's y position
    int alive;              //1: alive, 0: dead
    float chance_recover;   //Agent's chances to recover
};

struct global_variables
{
    //Struct with the global variables for the program.   

    int total_sick;    //Total of sick agents
    int total_immune;  //Total of immune agents 
    int total_healthy; //Total of healthy agents
    int alive_count;   //Total of alive agents
};

//Fucntion to create an agent.
struct agent_vars setup_agents(int lifespan, int carrying_capacity);
//Function to generate a random float value.
float RandomFloat(int a, int b);
//Function to generate a random position for the agent.
int random_pos();
//Function to create an amount of agents.
struct agent_vars* create_population();
//Function that sicks an agent.
void get_sick(struct agent_vars *agent);
//Function that cures an agent.
void get_healthy(struct agent_vars *agent);
//Function that sets an agents as immune
void become_immune(struct agent_vars *agent);
//Function that kills an agente
void get_killed(int position,struct agent_vars* population);
//Function that increases the differents times of an agent.
void get_older(struct agent_vars* population, int carrying_capacity, int lifespan);
//Function that changes randomly an agent's position
void move(struct agent_vars* population, int carrying_capacity);
//Function that updates the global variables in the program.
void update_global_variables(struct agent_vars* population, int carrying_capacity, struct global_variables *vars);
//Function that check if there's space for a new agent and if there's someona that can reproduce.
void reproduce(struct agent_vars* population, int lifespan, int carrying_capacity);
//Function that checks if a sick agent overpassed the sick time and if it has chances to recover.
void recover_or_die(struct agent_vars* population, int carrying_capacity);
//Function that prints the output of the program.
void printer(int max_weeks, int carrying_capacity, int total_sick, int total_immune, int total_healthy, int total_agents, int week_num);
//Function that infects an agent if if there's a sick one aorund it.
void infect(struct agent_vars* population, int total_agents);

#endif