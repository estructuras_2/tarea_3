#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <pthread.h>
#include <unistd.h>
#include <basic_model.h>

int main(int argc, char * argv []){

    /*  Global variables  */
    double timespent = 0.0;                     //Global variable for storing the time counter.
    clock_t begin = clock();                    //Begining of the execution time counter
    int lifespan = atoi(argv[1]);               //Lifespan of te agents as param
    int max_weeks = atoi(argv[2]);              //Ammount of ticks in the run as param
    int carrying_capacity = atoi(argv[3]);      //Carrying_capacity of ticks in the run as param.
    int week_num = 0;                           //Number of week (tick) of the iteration process.

    /*  Initialization of the population  */
    struct global_variables vars;               //struct with the final status of the agents (sick, healthy, immune and total).
    struct agent_vars *population = create_population(lifespan, carrying_capacity);     //initialization of the population structure.
    
    /*  Threading variables   */
    pthread_t threads1;         //Thread 1 for
    pthread_t threads2;         //Thread 2 for
    pthread_t threads3;         //Thread 3 for mutex process
    pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;   //Definition of the mutex lock.
    pthread_mutex_init(&lock, NULL);                    //Initialization of the mutex lock.
    struct thread_args str;     //Struct with input parameters for 
    struct agent_args str2;

    /*  Main loop   */
    for(int j = 0; j < max_weeks; j++ ){  
        week_num = j;
        str.population = population;
        str.carrying_capacity = carrying_capacity;
        str2.population = population;
        str2.carrying_capacity = carrying_capacity;
        str2.lifespan = lifespan;
        infect(population, carrying_capacity);
        pthread_create(&threads1, NULL, &recover_or_die, (void *)&str);
        pthread_create(&threads3, NULL, &get_older, (void *)&str2);
        reproduce(population, lifespan, carrying_capacity);
        update_global_variables(population, carrying_capacity, &vars);
        pthread_create(&threads2, NULL, &move, (void *)&str);
        printer(max_weeks, carrying_capacity, vars.total_sick, vars.total_immune, vars.total_healthy, vars.alive_count, week_num);
    }
    clock_t end = clock();
    timespent += (double)(end-begin)/CLOCKS_PER_SEC;
    printf("Execution time: %f s \n",timespent);
    pthread_join(threads1, NULL);
    pthread_join(threads2, NULL);
    pthread_join(threads3, NULL);
    pthread_mutex_destroy(&lock);
    return 0;
}