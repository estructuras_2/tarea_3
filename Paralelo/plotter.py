import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

def get_data():
    '''
        This function iterates over the input file to extract the data required to plot the results of the model.
            Outputs:
                Lists: returns the lists with the data extracted from the input file
                    week_list: number of the weeks (ticks).
                    sick_list: number of the sick agents in the simulation.
                    immune_list: number of the immune agents in the simulation
                    healthy_list: number of the healthy agents in the simulation.
                    total_list: number of the total ammount of agents. 
                    max_weeks: number of iterations specified in the model execution.
                    carrying_capacity: maximum ammount of agents allowed in the grid.

    '''
    #Creating empty lists to plot every extracted value from input file
    sick_list = []
    immune_list = []
    healthy_list = []
    total_list = []
    week_list = []

    #File reading 
    f = open('simulation_report.txt', 'r+')
    #Iterating over every line in the input file
    for tick in f:
        # Special case (ocurs only once)
        if tick[0:17] == 'carrying capacity':
            carrying_capacity = tick[19:]
            carrying_capacity = int(carrying_capacity[:-1])

        # Special case (ocurs only once)
        if tick[0:9] == 'max_weeks':
            max_weeks = tick[11:]
            max_weeks = int(max_weeks[:-1])

        # Principal iteration (most recurent)
        if tick[0:5] == 'tick[':
            elem = tick.split(',')
            elem = list(map(lambda x:x.strip(),elem))   #Removing \n from elements
            temp = elem[0]
            week = temp.split(' ')
            week = week[0]
            week = week.split('[')
            week = week[1]
            week = week[:-1]

            # Appending extracted values to the lists.
            week_list.append(week)
            sick_list.append(int(temp[8:]))
            immune_list.append(int(elem[1]))
            healthy_list.append(int(elem[2]))
            total_list.append(int(elem[3]))
        
        #Any other line (invalid lines)
        else: pass  

    return week_list, sick_list, immune_list, healthy_list, total_list, max_weeks, carrying_capacity


def plotter(week, sick, immune, healthy, total, max_weeks, carrying_capacity):
    '''
        this function gets all the inputs from the previous function and plots with scatter pyplot.
    '''
    # initializing the plotting space
    plt.figure(figsize=(4,3), dpi=50)
    manager = plt.get_current_fig_manager()
    
    #Plotting the lists obtained with the get_data function.
    sick_plot = plt.scatter(week, sick, c= ['r'], cmap='cool')
    immune_plot = plt.scatter(week, immune, c=['c'], cmap='cool')
    healthy_plot = plt.scatter(week, healthy, c=['g'], cmap='cool')
    total_plot = plt.scatter(week, total, c=['b'], cmap='cool')

    #Setting the plot visual window parameters.
    plt.axis([0, max_weeks, 0, carrying_capacity])  #x_min, x_max, y_min, y_max
    plt.tight_layout()
    plt.title(f"Populations")
    plt.xlabel("weeks")
    plt.ylabel("people")
    plt.grid(True)
    plt.legend((sick_plot, immune_plot, healthy_plot, total_plot),('sick agents', 'immune agents', 'healthy agents', 'total agents'))
    plt.savefig(f'plotted_sim.png', bbox_inches='tight')
    plt.show()


def main():
    week, sick, immune, healthy, total, max_weeks, carrying_capacity = get_data()
    plotter(week, sick, immune, healthy, total, max_weeks, carrying_capacity)

if __name__ == "__main__":
    main()