# Tarea3 Estructuras de Computadores Digitales II

* Emmanuel Rivel Montero
* Gabriel Gutiérrez Arguedas 
________________________________________________
________________________________________________

# Instrucciones de uso:
A continuación se muestran los comandos que debe ejecutar según la operación que desee realizar. Se
incluye un archivo Makefile; para cambir los parámetros de la simulación proceda a editar la línea
35 del archivo mencionado

#### Compilar el programa
    $ make
    
#### Ejecutar el programa
    $ make run

#### Limpieza de directorios
    $ make clean

#### Gráficos con los resultados
    $ python3 plotter.py